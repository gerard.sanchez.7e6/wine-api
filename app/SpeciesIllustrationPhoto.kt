data class SpeciesIllustrationPhoto(
    val alt: String,
    val src: String,
    val title: String
)