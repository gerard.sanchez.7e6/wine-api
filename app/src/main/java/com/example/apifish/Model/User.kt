package com.example.apifish.Model

data class User(val id: Long, var name: String, var url: String)
