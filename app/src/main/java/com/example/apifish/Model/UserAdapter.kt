package com.example.apifish.Model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apifish.Api.DataItem
import com.example.apifish.R
import com.example.apifish.View.FavouritesFragment
import com.example.apifish.View.RecyclerViewFragment
import com.example.apifish.databinding.ItemuserBinding

class UserAdapter(private val users: List<DataItem>, private val listener: RecyclerViewFragment):
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemuserBinding.bind(view)
        fun setListener(user: DataItem){
            binding.root.setOnClickListener {
                listener.onClick(user)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.itemuser, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]
        with(holder){
            setListener(user)
            binding.wineName.text = user.wine
            binding.wineId.text = user.id.toString()
            Glide.with(context)
                .load(user.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imgexample)
        }
    }


}
