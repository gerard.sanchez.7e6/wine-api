package com.example.apifish.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apifish.Api.DataItem
import com.example.apifish.Model.UserAdapter
import com.example.apifish.databinding.FragmentFavouritesBinding


class FavouritesFragment : Fragment() {


    lateinit var binding: FragmentFavouritesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        binding = FragmentFavouritesBinding.inflate(layoutInflater)
        return binding.root

    }



}