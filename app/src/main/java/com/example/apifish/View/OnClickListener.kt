package com.example.apifish.View

import com.example.apifish.Api.DataItem

interface OnClickListener {
    fun onClick(user: DataItem)
}
