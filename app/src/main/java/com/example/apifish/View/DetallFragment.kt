package com.example.apifish.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apifish.Api.DataItem
import com.example.apifish.PatroSingleton.ContactApplication
import com.example.apifish.R
import com.example.apifish.RoomDatabase.Whitewines
import com.example.apifish.ViewModel.UsersViewModel

import com.example.apifish.databinding.FragmentDetallBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class DetallFragment : Fragment() {

    lateinit var binding: FragmentDetallBinding
    lateinit var wineDetails : DataItem

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetallBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val usersTagsViewModel = ViewModelProvider(requireActivity())[UsersViewModel::class.java]

        usersTagsViewModel.wineDetails.observe(viewLifecycleOwner){
            wineDetails = it

            Glide.with(requireContext())
                .load(wineDetails.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imgexample)
            binding.wineId.text = wineDetails.id.toString()
            binding.wineName.text = wineDetails.wine
            binding.winery.text = wineDetails.winery
            binding.winelocation.text = wineDetails.location.replace("·",". ").replace("\n","")
            binding.wineaverage.text = wineDetails.rating.average
            binding.winereview.text = wineDetails.rating.reviews

        }
        binding.buttonvolver.setOnClickListener {
            findNavController().navigate(R.id.action_detailFragment_to_recyclerViewFragment)

        }
        var background = 0
        binding.estrellafav.setOnClickListener {

            val wineId = binding.wineId.text.toString().trim()
            val wineName = binding.wineName.text.toString().trim()
            val winery = binding.winery.text.toString().trim()
            val wineLocation = binding.winelocation.text.toString().trim()
            val image = binding.imgexample.toString().trim()
            val newWine = Whitewines(id = wineId.toLong(), wine = wineName, winery = winery, location = wineLocation, image = image)


            if (background == 0){
                binding.estrellafav.setBackgroundResource(R.drawable.estrellallena)
                CoroutineScope(Dispatchers.IO).launch {
                    ContactApplication.database.contactDao().addContact(newWine)
                }
                background++
            }
            else{
                binding.estrellafav.setBackgroundResource(R.drawable.estrellafavoritos)
                CoroutineScope(Dispatchers.IO).launch {
                    ContactApplication.database.contactDao().deleteContact(newWine)
                }
                background--
            }

        }

    }
}
