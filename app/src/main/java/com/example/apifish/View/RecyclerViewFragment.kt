package com.example.apifish.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apifish.*
import com.example.apifish.Api.DataItem
import com.example.apifish.Model.User
import com.example.apifish.Model.UserAdapter
import com.example.apifish.ViewModel.UsersViewModel
import com.example.apifish.databinding.FragmentRecyclerViewBinding


class RecyclerViewFragment : Fragment(), OnClickListener {

    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding
    private val viewModel : UsersViewModel by activityViewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val usersViewModel = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        usersViewModel.data.observe(viewLifecycleOwner) {
            setupRecyclerView(it as ArrayList<DataItem>)

        }

    }

    private fun setupRecyclerView(lista: ArrayList<DataItem>) {
        userAdapter = UserAdapter(lista, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    private fun getUsers(): MutableList<User>{
        val users = mutableListOf<User>()
        users.add(User(1, "Nom", "url_imatge"))
        return users
    }


    override fun onClick(user: DataItem) {
        val usersViewModel = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        usersViewModel.wineDetails.postValue(user)
        findNavController().navigate(R.id.action_recyclerViewFragment_to_detailFragment)
    }


}