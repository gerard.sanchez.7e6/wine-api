package com.example.apifish.View

class Repository {
    val apiInterface = ApiInterface.create()

    suspend fun getTags() = apiInterface.getTags()

}
