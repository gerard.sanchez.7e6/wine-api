package com.example.apifish.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.apifish.Api.DataItem
import com.example.apifish.View.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UsersViewModel: ViewModel() {

    private val repository = Repository()
    var data = MutableLiveData<List<DataItem>>()
    var wineDetails = MutableLiveData<DataItem>()

    init {
        fetchData()
    }

    fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getTags()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    data.postValue(response.body())
                } else {
                    Log.e("Error :", response.message())
                }
            }
        }
    }


}

