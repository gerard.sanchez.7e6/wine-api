package com.example.apifish.RoomDatabase


import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.apifish.Dao.ContactDao

@Database(entities = arrayOf(Whitewines::class), version = 1)
abstract class ContactDatabase: RoomDatabase() {
    abstract fun contactDao(): ContactDao
}

