package com.example.apifish.RoomDatabase

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Whitewines")
 data class Whitewines(
   @PrimaryKey(autoGenerate = true) var id: Long = 0,
   val image: String,
   val location: String,
   val wine: String,
   val winery: String
    )
