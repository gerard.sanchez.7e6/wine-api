package com.example.apifish.PatroSingleton

import android.app.Application
import androidx.room.Room
import com.example.apifish.RoomDatabase.ContactDatabase


class ContactApplication: Application() {
    companion object {
        lateinit var database: ContactDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            ContactDatabase::class.java,
            "ContactDatabase").build()
    }
}
