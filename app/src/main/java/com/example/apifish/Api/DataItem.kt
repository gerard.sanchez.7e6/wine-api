package com.example.apifish.Api

data class DataItem(
    val id: Int,
    val image: String,
    val location: String,
    val rating: Rating,
    val wine: String,
    val winery: String
)