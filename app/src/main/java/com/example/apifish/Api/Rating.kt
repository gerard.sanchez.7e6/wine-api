package com.example.apifish.Api

data class Rating(
    val average: String,
    val reviews: String
)