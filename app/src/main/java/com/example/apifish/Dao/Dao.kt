package com.example.apifish.Dao

import androidx.room.Dao
import androidx.room.*
import com.example.apifish.RoomDatabase.Whitewines


@Dao
interface ContactDao {
    @Query("SELECT * FROM Whitewines")
    fun getAllContacts(): MutableList<Whitewines>
    @Query("SELECT * FROM Whitewines where wine = :contactName")
    fun getContactsByName(contactName: String): MutableList<Whitewines>
    @Insert
    fun addContact(contactEntity: Whitewines)
    @Update
    fun updateContact(contactEntity: Whitewines)
    @Delete
    fun deleteContact(contactEntity: Whitewines)
}

